import './App.css';
import HeaderImg from './assets/images/header.jpg'
import CoreValueImg from './assets/images/core-values.png'
import { Disclosure } from '@headlessui/react'
import { Bars3Icon, XMarkIcon, ArrowLeftIcon, ArrowRightIcon, ChartBarIcon, MinusIcon, ChevronDoubleDownIcon } from '@heroicons/react/24/outline'
import { useEffect, useState } from 'react';

const navigation = [
  { name: 'Home', href: '#', current: true },
  { name: 'Who We Are', href: '#', current: false },
  { name: 'Our Values', href: '#', current: false },
  { name: 'The Perks', href: '#', current: false },
]

const introData = [
  { 
    title: 'Who we are', 
    subtitle: 'Technology Company', 
    description: 'Sed ut perspiciatis unde omnis iste natus sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.',
    current: true
  },
  {
    title: 'What we do',
    subtitle: 'Professional Brand Management',
    description: 'Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.',
    current: false
  },
  {
    title: 'How we do',
    subtitle: 'Strategize, Design, Collaborate',
    description: 'Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse sequam nihil molestiae consequatur.',
    current: false
  }
]

const specialityData = [
  {
    title: "Exhaust",
    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis euismod libero vel leo auctor, in venenatis nulla consequat. Sed commodo nunc sit amet congue aliquam.",
    image: require("./assets/images/exhaust.png"),
    current: false
  },
  {
    title: "Speed Improvement",
    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis euismod libero vel leo auctor, in venenatis nulla consequat. Sed commodo nunc sit amet congue aliquam.",
    image: require("./assets/images/speed.png"),
    current: true
  },
  {
    title: "Accesories",
    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis euismod libero vel leo auctor, in venenatis nulla consequat. Sed commodo nunc sit amet congue aliquam.",
    image: require("./assets/images/accesories.png"),
    current: false
  }
]

function classNames(...classes) {
  return classes.filter(Boolean).join(' ')
}

const App = () => { 
  const [intro, setIntro] = useState([])
  const [speciality, setSpeciality] = useState([])
  const [activeIntro, setActiveIntro] = useState(0)
  const [activeSpeciality, setActiveSpeciality] = useState(0)

  useEffect(() => {
    // set data intro
    setIntro(introData)
    setSpeciality(specialityData)
  }, [])
  
  const nextIntro = () => {
    let currentIndex = activeIntro
    let newIndex = 0
  
    // if the last intro is true, back to first intro
    if(activeIntro === (intro.length - 1)) {
      setActiveIntro(0)
    } else {
      newIndex = currentIndex + 1
      setActiveIntro((state) => state + 1)
    }

    // change object value
    setIntro((state) => {
      state[currentIndex].current = false
      state[newIndex].current = true

      return state
    })
  }

  const nextSpeciality = () => {
    let currentIndex = activeSpeciality
    let newIndex = 0
  
    // if the last speciality is true, back to first speciality
    if(activeSpeciality === (intro.length - 1)) {
      setActiveSpeciality(0)
    } else {
      newIndex = currentIndex + 1
      setActiveSpeciality((state) => state + 1)
    }

    // change object value
    setSpeciality((state) => {
      state[currentIndex].current = false
      state[newIndex].current = true

      return state
    })
  }

  const prevIntro = () => {
    let currentIndex = activeIntro
    let newIndex = 0
  
    // if the first intro is true, back to last intro
    if(activeIntro === 0) {
      newIndex = intro.length - 1
      setActiveIntro(intro.length - 1)
    } else {
      newIndex = currentIndex - 1
      setActiveIntro((state) => state - 1)
    }

    // change object value
    setIntro((state) => {
      state[currentIndex].current = false
      state[newIndex].current = true

      return state
    })
  }
  
  const prevSpeciality = () => {
    let currentIndex = activeSpeciality
    let newIndex = 0
  
    // if the first speciality is true, back to last speciality
    if(activeSpeciality === 0) {
      newIndex = speciality.length - 1
      setActiveSpeciality(speciality.length - 1)
    } else {
      newIndex = currentIndex - 1
      setActiveSpeciality((state) => state - 1)
    }

    // change object value
    setSpeciality((state) => {
      state[currentIndex].current = false
      state[newIndex].current = true

      return state
    })
  }

  const Header = () => {
    return (
      <div className="min-h-full z-40">
        <Disclosure as="nav">
          {({ open }) => (
            <>
              <div className="mx-auto max-w-7xl px-4 sm:px-6 lg:px-8">
                <div className="flex h-16 items-center justify-between">
                  <div className="flex items-center justify-between w-full">
                    <div className="flex-shrink-0">
                      <div className="flex items-center">
                        <ChartBarIcon className="block w-6 h-6 text-sky-500"/>
                        <span className="text-sky-500 font-bold ml-2 text-xl">COMPANY</span>
                      </div>
                    </div>
                    <div className="hidden md:block">
                      <div className="ml-10 flex items-baseline space-x-4">
                        {navigation.map((item) => (
                          <a
                            key={item.name}
                            href={item.href}
                            className={classNames(
                              item.current
                                ? 'text-sky-500'
                                : 'text-black hover:text-sky-500',
                              'px-3 py-2 rounded-md text-sm font-medium'
                            )}
                            aria-current={item.current ? 'page' : undefined}
                          >
                            {item.name}
                          </a>
                        ))}
                      </div>
                    </div>
                  </div>
                  <div className="-mr-2 flex md:hidden">
                    {/* Mobile menu button */}
                    <Disclosure.Button className="inline-flex items-center justify-center rounded-md p-2 text-black hover:text-sky-500 focus:outline-none focus:ring-2 focus:ring-white focus:ring-offset-2 focus:ring-offset-gray-800">
                      <span className="sr-only">Open main menu</span>
                      {open ? (
                        <XMarkIcon className="block h-6 w-6" aria-hidden="true" />
                      ) : (
                        <Bars3Icon className="block h-6 w-6" aria-hidden="true" />
                      )}
                    </Disclosure.Button>
                  </div>
                </div>
              </div>

              <Disclosure.Panel className="md:hidden">
                <div className="space-y-1 px-2 pt-2 pb-3 sm:px-3">
                  {navigation.map((item) => (
                    <Disclosure.Button
                      key={item.name}
                      as="a"
                      href={item.href}
                      className={classNames(
                        item.current ? 'text-white bg-sky-500' : 'text-black hover:text-sky-500',
                        'block px-3 py-2 text-base font-medium'
                      )}
                      aria-current={item.current ? 'page' : undefined}
                    >
                      {item.name}
                    </Disclosure.Button>
                  ))}
                </div>
              </Disclosure.Panel>
            </>
          )}
        </Disclosure>
      </div>
    )
  }

  return (
    <div className="overflow-x-hidden">
      <Header />
      <section className="banner flex flex-col md:flex-row justify-between">
        <div className="banner-image md:w-3/6 w-100">
          <img src={HeaderImg} alt="header" className="h-auto w-auto"/>
        </div>
        <div className="banner-description relative md:w-3/6 h-100">
          <div className="bg-[#4097DB] sm:hidden md:hidden absolute w-[30rem] h-28 rotate-12 z-20 -left-12 -top-8"></div>
          <div className="bg-[#53A7E9] sm:hidden md:hidden absolute w-[30rem] h-28 -rotate-12 z-10 -right-12 -top-8"></div>
          <div className="bg-[#4097DB] relative w-full h-full p-10 flex flex-col justify-center z-40">
            <h3 className="text-white text-4xl font-bold">Discover Your Wonder</h3>
            <p className="text-white mt-4 font-thin">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
          </div>
          <div className="w-full h-auto flex flex-col items-center md:hidden">
            <div className="h-10 w-10 absolute rounded-full -bottom-5 z-50 bg-white hover:bg-gray-200 drop-shadow-md cursor-pointer flex flex-col items-center justify-center">
              <ChevronDoubleDownIcon className="block h-5 w-5"/>
            </div>
          </div>
        </div>
      </section>
      <section className="intro relative z-50 p-10 min-h-[460px] md:min-h-0 max-h-[460px]">
        <div className="intro-body">
          {
            intro.map(item => {
              if(item.current) {
                return (
                  <div className="intro-item">
                    <h3 className="intro-title text-3xl text-sky-500 font-bold">{item.title}</h3>
                    <p className="intro-subtitle text-1xl font-bold mt-4">{item.subtitle}</p>
                    <p className="intro-description text-gray-500 mt-2">{item.description}</p>
                  </div>
                )
              }
            })
          }
        </div>
        <div className="intro-navigation flex items-center justify-between mt-16">
          <div className="pagination">
            <span className="font-bold text-2xl">{('0' + (activeIntro+1)).slice(-2)}</span>
            <span className="font-bold text-2xl text-gray-400 mx-1">/</span>
            <span className="font-bold text-gray-400">{('0' + intro.length).slice(-2)}</span>
          </div>
          <div className="nav-arrow flex">
            <ArrowLeftIcon 
              className="nav-previous block h-10 w-10 text-gray-500 bg-gray-200 hover:bg-gray-300 p-3 cursor-pointer" 
              aria-hidden="true" 
              onClick={() => prevIntro()}
            />
            <ArrowRightIcon 
              className="nav-next block h-10 w-10 text-white bg-sky-500 hover:bg-sky-600 p-3 cursor-pointer" 
              aria-hidden="true" 
              onClick={() => nextIntro()}
            />
          </div>
        </div>
      </section>
      <section className="our-core bg-gray-100">
        <h3 className="intro-title text-3xl text-sky-500 text-center font-bold mb-4 pt-10">Our Core Values</h3>
        <p className="text-gray-500 md:text-center mb-4 px-10">Ridiculus laoreet libero pretium et, sit vel elementum convallis fames. Sit suspendisse etiam eget egestas. Aliquet odio et lectus etiam sit.</p>
        <p className="text-gray-500 md:text-center px-10">In mauris rutrum ac ut volutpat, ornare nibh diam. Montes, vitae, nec amet enim.</p>
        <div className="mt-8 px-10 md:flex md:justify-between md:gap-5">
          <div className="our-core-item my-8">
            <div className="flex items-center mb-3 -ml-4 md:ml-0">
              <MinusIcon className="block w-6 h-6 mr-3 md:hidden"/>
              <h4 className="text-2xl">Dedication</h4>
            </div>
            <p className="text-gray-500 ml-5 md:ml-0">Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat.</p>
          </div>
          <div className="our-core-item my-8">
            <div className="flex items-center mb-3 -ml-4 md:ml-0">
              <MinusIcon className="block w-6 h-6 mr-3 md:hidden"/>
              <h4 className="text-2xl">Intellectual Honesty</h4>
            </div>
            <p className="text-gray-500 ml-5 md:ml-0">Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias conse.</p>
          </div>
          <div className="our-core-item my-8">
            <div className="flex items-center mb-3 -ml-4 md:ml-0">
              <MinusIcon className="block w-6 h-6 mr-3 md:hidden"/>
              <h4 className="text-2xl">Curiosity</h4>
            </div>
            <p className="text-gray-500 ml-5 md:ml-0">At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque.</p>
          </div>
        </div>
        <div className="mt-4 flex justify-end">
          <img src={CoreValueImg} className="md:w-auto w-[90%] -mr-16" alt="core value"/>
        </div>
      </section>
      <section className="bg-[#509FDD] px-5 py-10">
        <div className="bg-white p-8">
          <h3 className="intro-title text-2xl text-sky-500 md:text-center font-black mb-4">OUR SPECIALITY</h3>
          <p className="text-gray-500 md:text-center">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis euismod libero vel leo auctor, in venenatis nulla consequat. Sed commodo nunc sit amet congue aliquam.</p>
          <div className="speciality mt-5">
            <div className="speciality-item-desktop justify-between gap-6 md:flex hidden">
              {
                speciality.map(item => (
                  <div className="text-center flex flex-col items-center">
                    <img src={item.image} alt={item.tile}/>
                    <p className="text-black font-bold mt-3">{item.title}</p>
                    <p className="text-gray-400 mt-5">{item.description}</p>
                  </div>
                ))
              }
            </div>
            <div className="speciality-item-mobile md:hidden">
              {
                speciality.map(item => {
                  if(item.current) {
                    return (
                      <div className="text-center flex flex-col items-center">
                        <img src={item.image} alt={item.tile}/>
                        <p className="text-black font-bold mt-3">{item.title}</p>
                        <p className="text-gray-400 mt-5">{item.description}</p>
                      </div>
                    )
                  }
                })
              }
            </div>
            <div className="speciality-navigation flex justify-between mt-8 md:hidden">
              <ArrowLeftIcon 
                className="block h-6 w-6 text-[#3D46A2] hover:text-[#D2D2D2] cursor-pointer" 
                aria-hidden="true" 
                onClick={() => prevSpeciality()}
              />
              <div className="pagination flex items-center gap-3">
                {
                  speciality.map(item => {
                    if(item.current) {
                      return (
                        <div className="w-3 h-3 rounded-full bg-white border-2 border-[#3D46A2]"></div>
                      )
                    } else {
                      return (
                        <div className="w-3 h-3 rounded-full bg-[#DAF3FC]"></div>
                      )
                    }
                  })
                }
              </div>
              <ArrowRightIcon 
                className="block h-6 w-6 text-[#3D46A2] hover:text-[#D2D2D2] cursor-pointer" 
                aria-hidden="true"  
                onClick={() => nextSpeciality()}
              />
            </div>
          </div>
        </div>
      </section>
      <section className="footer px-5 py-10 bg-cover">
        <div className="flex mb-5">
          <ChartBarIcon className="block w-6 h-6 text-white" />
          <span className="text-white font-bold ml-2 text-xl">COMPANY</span>
        </div>
        <div className="md:flex md:items-start md:gap-10">
          <div className="bg-white p-5">
            <select className="mb-6 w-full font-bold text-[#00537C] text-sm border-gray-400 p-3">
              <option value="technology">Technology Department</option>
              <option value="marketing">Marketing Department</option>
              <option value="sales">Sales Department</option>
            </select>
            <p className="text-[#25A0D8] text-md">Jl. Lembong No 8 Kel. Braga Kec. Sumur Bandung, Kota Bandung, Jawa Barat</p>
          </div>
          <div className="footer-menu md:mt-0 mt-6 mb-24">
            <ul className="list-none flex flex-col">
              {
                navigation.map((item, index) => {
                  if(index !== 0) {
                    return (
                      <li className="text-white mb-5 cursor-pointer inline hover:underline">{item.name}</li>
                    )
                  }
                })
              }
            </ul>
          </div>
        </div>
      </section>
    </div>
  )
}

export default App;
