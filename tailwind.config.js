/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        'blue-1': '#53A7E9',
        'blue-2': '#4097DB'
      }
    },
  },
  plugins: [
    require('@tailwindcss/forms')
  ],
}
